# frozen_string_literal: true

class VAL::Plugins::Core
    MESSAGE_TIMEOUT = 30

    def cmd_help(event, *args)
        message =
            if args.empty?
                command_list(event)
            elsif /\p{upper}/ =~ args.first
                plugin_info(event, args.first)
            else
                command_info(event, args.first.to_sym)
            end

        event.channel.send_temporary_message(message, MESSAGE_TIMEOUT) unless message.nil?

        Thread.new do
            sleep(MESSAGE_TIMEOUT)
            event.message.delete
        end

        nil
    end

    private

    def command_info(event, name)
        return "Command #{name} not found." unless @bot.commands.key?(name)

        cmd = @bot.commands[name]

        desc  = cmd.attributes[:description] || 'No description available.'
        usage = cmd.attributes[:usage] || 'Usage:Not available.'

        event.send_temporary_embed do |embed|
            embed.title       = "Command: #{name}"
            embed.description = desc

            usages = usage.split("\n")

            usages.each do |line|
                title, value = line.split(':')
                embed.add_field name: title, value: "`#{value}`"
            end
        end
    end

    def plugin_info(event, name)
        return "Plugin #{name} not found." unless @manager.plugins_enabled.key?(name)

        plug = @manager.plugins_enabled[name]

        event.send_temporary_embed do |embed|
            embed.title = "Plugin: #{name}"
            embed.description = plug.description
            footer_text = "Use '#{@bot.prefix}help command' for command info."
            embed.footer = EmbedFooter.new(text: footer_text)

            plug.commands.each do |cmd_name, cmd|
                desc = cmd.attributes[:description] || 'No description available.'
                embed.add_field name: cmd_name, value: desc, inline: true
            end
        end
    end

    def command_list(event)
        event.send_temporary_embed do |embed|
            @manager.plugins_enabled.each do |plug_name, plug|
                begin
                    VAL.permitted?(event, permission: plug.permission)
                    next if plug.commands.nil?

                    cmds = plug.commands.keys.join(', ')
                    embed.add_field name: plug_name, value: cmds, inline: true
                rescue VAL::PermissionError
                    next
                end
            end
        end
    end
end
