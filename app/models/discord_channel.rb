# frozen_string_literal: true

class DiscordChannel < ActiveRecord::Base
    validates :cid,  presence: true, uniqueness: true
    validates :name, presence: true

    belongs_to :discord_server
end
