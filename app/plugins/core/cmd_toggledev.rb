# frozen_string_literal: true

class VAL::Plugins::Core
    def cmd_toggledev(event)
        if VAL::Config[:bot][:dev_mode]
            disable_dev_mode
            event << 'Developer mode disabled'
        else
            enable_dev_mode
            event << 'Developer mode enabled'
        end
    end

    private

    def enable_dev_mode
        VAL::Config[:bot][:dev_mode] = true
    end

    def disable_dev_mode
        VAL::Config[:bot][:dev_mode] = false
    end
end
