# frozen_string_literal: true

class VAL::Plugins::Links
    def cmd_linkadd(event, *desc, link)
        server_id = event.server.id

        link = Link.new do |l|
            l.server_id = server_id
            l.description = desc.join(' ')
            l.link = link
        end

        link.save

        'Link added.'
    end
end
