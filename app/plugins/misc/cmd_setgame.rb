# frozen_string_literal: true
module VAL::Plugins
    class Misc
        def cmd_setgame(event, *newgame)
            newgame = newgame.join(' ')
            event.bot.game = newgame

            msg = "New game set to `#{newgame}`."
            @log.info(msg)

            event << msg
        end
    end
end
