# frozen_string_literal: true
require 'open-uri'
require 'json'

module VAL::Plugins
    class Anime
        EmbedImage = Discordrb::Webhooks::EmbedImage
        EmbedThumb = Discordrb::Webhooks::EmbedThumbnail

        BASE_URL = 'https://kitsu.io/api/edge/users'

        def cmd_kitsu(event, user)
            response = open("#{BASE_URL}?filter[name]=#{user.downcase}")

            json =
                case response
                when Tempfile then JSON.parse(response.read)
                when StringIO then JSON.parse(response.string)
                end

            print_info(event, user, json['data'].first)
        rescue OpenURI::HTTPError => e
            @log.error(e.message)
            "Could not find user #{user}."
        end

        private

        def print_info(event, user, json)
            return unless json

            username = json['attributes']['name']
            avatar   = json['attributes']['avatar']['small']
            url      = "https://kitsu.io/users/#{user.downcase}"
            about    = json['attributes']['about']
            waifu    = get_waifu(json)
            extra    = {
                'Waifu'     => waifu[0] || 'None',
                'Favorites' => json['attributes']['favoritesCount'],
                'Reviews'   => json['attributes']['reviewsCount'],
                'Followers' => json['attributes']['followersCount']
            }

            event.channel.send_embed do |embed|
                embed.url         = url
                embed.title       = username
                embed.description = about
                embed.thumbnail   = EmbedThumb.new(url: waifu[1])
                embed.image       = EmbedImage.new(url: avatar)

                extra.each do |key, value|
                    embed.add_field name: key, value: value, inline: true
                end
            end
        end

        def get_waifu(json)
            waifu = json['relationships']['waifu']
            return unless waifu

            waifu_data = JSON.parse(open(waifu['links']['related']).string)
            if waifu_data['data']
                waifu_name  = waifu_data['data']['attributes']['name'] || nil
                waifu_image = waifu_data['data']['attributes']['image']['original'] || nil
                [waifu_name, waifu_image]
            else
                [nil, nil]
            end
        end
    end
end
