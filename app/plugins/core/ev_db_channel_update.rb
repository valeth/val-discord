# frozen_string_literal: true

class VAL::Plugins::Core
    def ev_channel_deleted(event)
        server_id  = event.server.id
        channel_id = event.id

        remove_channel_from_server(channel_id, server_id)
    end

    def ev_channel_created(event)
        server_id = event.server.id
        channel   = event.channel

        add_channel_to_server(channel, server_id)
    end

    private

    def add_channel_to_server(channel, server_id)
        ar_server = DiscordServer.find_by(sid: server_id)
        return unless ar_server

        ar_channel = DiscordChannel.find_by(cid: channel.id) || add_channel(channel)

        return if ar_server.discord_channels.find_by(cid: channel.id)
        ar_server.discord_channels << ar_channel
    end

    def add_channel(channel)
        DiscordChannel.create do |c|
            c.cid  = channel.id
            c.name = channel.name
        end
    end

    def remove_channel_from_server(channel_id, server_id)
        ar_server = DiscordServer.find_by(sid: server_id)
        return unless ar_server

        ar_channel = ar_server.discord_channels.find_by(cid: channel_id)
        return unless ar_channel

        ar_server.discord_channels.delete(ar_channel)
        ar_channel.delete
    end
end
