# frozen_string_literal: true

require 'active_support'
require 'yaml'

require 'discordrb'
require_relative 'val/discordrb/channel'
require_relative 'val/discordrb/events/message'

require_relative 'val/config'
require_relative 'val/logger'
require_relative 'val/database'
require_relative 'val/permission'
require_relative 'val/cache'
require_relative 'val/utils'
require_relative 'val/plugin_manager'
require_relative 'val/bot'

module VAL
end
