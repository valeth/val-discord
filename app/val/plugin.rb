# frozen_string_literal: true

require_relative 'command'
require_relative 'event'
require 'active_support'

module VAL
    class Plugin
        include Discordrb::Commands::CommandContainer
        include Discordrb::EventContainer

        PluginError = Class.new(StandardError)
        ConfigError = Class.new(PluginError)
        Disabled    = Class.new(PluginError)

        attr_reader :name
        attr_reader :description
        attr_reader :permission

        def self.load_config_file(manager, bot, path)
            config = YAML.load_file("#{path}/plugin.yml")
            raise ConfigError, 'Failed to parse YAML file!' unless config
            raise ConfigError, 'Plugin name not set!' if config[:name].nil?

            attributes = VAL::Config[:plugin].dup
            attributes[:file] ||= "#{config[:name].downcase}.rb"
            attributes[:path]    = path
            attributes[:manager] = manager
            attributes[:bot]     = bot
            attributes.update(config)

            OpenStruct.new(attributes)
        end

        def self.plugin_class(name)
            "VAL::Plugins::#{name}".constantize
        end

        def self.from_path(manager, bot, path)
            config = load_config_file(manager, bot, path)

            require "#{config.path}/#{config.file}"

            plugin_class(config.name).new(config)
        end

        def initialize(config)
            @name            = config.name
            @file            = config.file
            @path            = config.path
            @permission      = config.permission
            @description     = config.description
            @bot             = config.bot
            @manager         = config.manager
            @commands_config = config.commands
            @events_config   = config.events
            @enabled         = config.enabled
            @events          = {}

            @log = VAL::Logger["Plugins/#{@name}"]

            raise Disabled, "Plugin #{@name} is disabled." unless @enabled

            enable

            @bot.include! self
        end

        def disable
            unless @commands.nil?
                @commands.keys.each do |name|
                    @bot.remove_command(name)
                    remove_command(name)
                end
            end

            @events.values.each do |ev|
                ev.values.each do |handler|
                    @bot.remove_handler(handler)
                end
            end

            @events = {}
            @enabled = false
        end

        def enable
            load_rate_limiters
            load_commands
            load_events

            @enabled = true
        end

        private

        def load_config(thing, config, type:)
            attributes = {
                enabled:    @enabled,
                file:       @file,
                permission: @permission,
                func:       "#{type}_#{thing}".to_sym
            }

            attributes[:types]   = [] if type == :ev
            attributes[:options] = {} if type == :cmd
            attributes.update(config) unless config.nil?

            OpenStruct.new(attributes)
        end

        def load_rate_limiters
            bucket :user, limit: 3, time_span: 60, delay: 10
        end
    end
end
