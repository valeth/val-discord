# frozen_string_literal: true

class VAL::Plugins::Core
    def ev_member_left(event)
        server = event.server
        user   = event.user

        remove_user_from_server(user, server)
    end

    def ev_member_joined(event)
        server = event.server
        user   = event.user

        add_user_to_server(user, server)
    end

    private

    def add_user_to_server(user, server)
        ar_server = DiscordServer.find_by(sid: server.id)
        return unless ar_server

        ar_user = DiscordUser.find_by(uid: user.id) || add_user(user)

        return if ar_server.discord_users.find_by(uid: user.id)
        ar_server.discord_users << ar_user
    end

    def add_user(user)
        DiscordUser.create do |u|
            u.uid  = user.id
            u.name = user.name
        end
    end

    def remove_user_from_server(user, server)
        ar_server = DiscordServer.find_by(sid: server.id)
        return unless ar_server

        ar_user = ar_server.discord_users.find_by(uid: user.id)
        return unless ar_user

        ar_server.discord_users.delete(ar_user)

        remove_user(user)
    end

    def remove_user(user)
        ar_user = DiscordUser.find_by(uid: user.id)
        return unless ar_user
        return unless ar_user.discord_servers.size.zero?
        ar_user.delete
    end
end
