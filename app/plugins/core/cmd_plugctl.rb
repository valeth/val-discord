# frozen_string_literal: true

class VAL::Plugins::Core
    def cmd_disable(event, *args)
        return if args.empty?

        plugin = args.first
        return "Can't disable this plugin." if plugin == @name
        username = event.author.name

        return 'Plugin already disabled' unless @manager.plugin_enabled?(plugin)

        @manager.disable_plugin(plugin)

        msg = "Plugin `#{plugin}` disabled by **#{username}**"
        @log.info(msg)
        event << msg
    end

    def cmd_enable(event, *args)
        return if args.empty?

        plugin = args.first
        username = event.author.name

        return 'Plugin already enabled' if @manager.plugin_enabled?(plugin)
        @manager.enable_plugin(plugin)

        msg = "Plugin `#{plugin}` enabled by **#{username}**"
        @log.info(msg)
        event << msg
    end
end
