# frozen_string_literal: true

class VAL::Plugins::Utils
    def cmd_setname(event, *new_name)
        bot_profile = event.bot.profile

        bot_profile.username = new_name.join(' ')
        event << 'bot username updated'
    end
end
