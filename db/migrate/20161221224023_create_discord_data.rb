# frozen_string_literal: true

class CreateDiscordData < ActiveRecord::Migration[5.0]
    def change
        create_table :discord_users do |t|
            t.integer    :uid, limit: 8, null: false
            t.string     :name, null: false
            t.timestamps
        end

        create_table :discord_servers do |t|
            t.integer    :sid, limit: 8, null: false
            t.string     :name, null: false
            t.timestamps
        end

        create_table :discord_memberships do |t|
            t.belongs_to :discord_server, index: true
            t.belongs_to :discord_user,   index: true
            t.timestamps
        end

        create_table :discord_channels do |t|
            t.integer    :cid, limit: 8, null: false
            t.string     :name, null: false
            t.belongs_to :discord_server, index: true
            t.timestamps
        end
    end
end
