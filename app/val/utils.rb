# frozen_string_literal: true
module VAL
    module Utils
        def self.distribute(ary, size)
            tmp = []
            block = []

            ary.each.with_index(1) do |elem, i|
                block << elem if block.size < size

                if block.size == size || i == ary.size
                    tmp << block
                    block = []
                end
            end

            tmp
        end
    end
end
