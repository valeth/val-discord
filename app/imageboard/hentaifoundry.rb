# frozen_string_literal: true
module Hentaifoundry
    require 'imageboard'
    require 'open-uri'
    require 'mechanize'

    Imageboard.board :hentaifoundry do |args|
        url = 'http://www.hentai-foundry.com/search/pictures?enterAgree=1&query='
        url = URI.escape("#{url}#{args[:tags].join('+')}")
        agent = Mechanize.new
        tmp = []

        begin
            agent.get(url) do |page|
                regex = %r{/\/pictures\/user/}
                links = page.links_with(href: regex).uniq!(&:link.href)
                break if links.nil?

                links.each do |link|
                    picture = link.click.images_with(src: /pictures/)
                    tmp << "http:#{picture.first.src}" unless picture.first.nil?
                end
            end
        rescue OpenURI::HTTPError
            puts "failed to retrieve url:\n#{url}"
        end

        tmp
    end
end
