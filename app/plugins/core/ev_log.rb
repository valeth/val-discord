# frozen_string_literal: true

class VAL::Plugins::Core
    MLOG = VAL::Logger['VAL/Messages', targets: [:messages]]
    ELOG = VAL::Logger['VAL/Events', targets: [:stdout, :events]]

    def ev_log_channel(event)
        channel = event.name
        server  = event.server.name

        action =
            case event
            when ChannelUpdateEvent then 'updated'
            when ChannelDeleteEvent then 'deleted'
            when ChannelCreateEvent then 'created'
            else return
            end

        ELOG.info("#{action} channel #{channel} on #{server}")
    end

    def ev_log_member(event)
        server = event.server.name
        user   = event.user.name

        action =
            case event
            when ServerMemberAddEvent    then 'joined'
            when ServerMemberDeleteEvent then 'left'
            else return
            end

        ELOG.info("#{user} #{action} #{server}")
    end

    def ev_log_message(event)
        server  = event.server.name
        channel = event.channel.name
        user    = event.user.name

        action =
            case event
            when MessageEditEvent then 'edited'
            when MessageEvent     then 'wrote'
            else return
            end

        msg = format("%s @ %s # %s #{action}:\n%s",
                     user, server, channel, event.content)

        MLOG.info(msg)
    end

    def ev_log_presence(event)
        server  = event.server
        channel = server.default_channel.name
        user    = event.user.name

        ELOG.info("#{user} went online in #{channel}@#{server.name}")
    end
end
