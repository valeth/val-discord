# frozen_string_literal: true
module Rule34
    require 'imageboard'
    require 'open-uri'
    require 'nokogiri'

    Imageboard.board :rule34 do |args|
        url = 'http://rule34.paheal.net/post/list/'
        url = URI.escape("#{url}#{args[:tags].join(' ')}/#{args[:page]}")
        tmp = []

        begin
            doc = Nokogiri::HTML(open(url))
            tmp = doc.css('div.thumb').map do |elem|
                elem.css('a')[1]['href']
            end
        rescue OpenURI::HTTPError
            puts "failed to retrieve url:\n#{url}"
        end

        tmp
    end
end
