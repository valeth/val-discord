# frozen_string_literal: true
require 'safe_ruby'

module VAL::Plugins
    class Ruby
        def cmd_ruby(event, *args)
            handle_errors(event.channel) do
                evaluated = SafeRuby.eval(args.join(' '))
                send_embed(event.channel, evaluated.to_s, wrap: true)
            end
        end

        private

        def wrap_in_block(text, syntax: nil)
            "```#{syntax}\n#{text}\n```"
        end

        def send_embed(channel, msg, title: 'Evaluation successful', wrap: false)
            desc = wrap ? wrap_in_block(msg, syntax: 'ruby') : msg

            channel.send_embed do |embed|
                embed.title = title
                embed.description = desc
            end
        end

        def send_error_embed(channel, msg)
            msg = msg.split("\n").first.split(':').last
            send_embed(channel, msg, title: 'An error occured!')
        end

        def handle_errors(channel)
            yield
        rescue SafeRuby::EvalError => e
            send_error_embed(channel, e.message)
        end
    end
end
