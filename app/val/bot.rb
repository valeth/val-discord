# frozen_string_literal: true

module VAL
    AUTHORS = YAML.safe_load(open('AUTHORS'), [Symbol], [], true)[:authors]
    silence_warnings do
        Discordrb::LOGGER = Logger['Discordrb']
    end

    class Bot < Discordrb::Commands::CommandBot
        def initialize
            super(Config[:bot][:auth])

            trap('INT')  { exit(0) }
            trap('TERM') { exit(0) }

            at_exit { stop }

            @log = Logger['VAL']
            @log.info('Bot is in developer mode.') if Config[:bot][:dev_mode]
            @log.info('created bot')
            @running = false
            @plugman = VAL::PluginManager.new(self)
        end

        def run
            return if running?
            @log.info('starting bot')
            super :async
            @running = true
            @log.info('started bot')
        end

        def stop
            return unless running?
            super
            @log.info('stopping bot')
            @running = false
            @log.info('stopped bot')
        end

        def wait
            return unless running?
            sync
        end

        def start
            run
            wait
        end

        def running?
            @running
        end
    end
end
