# frozen_string_literal: true
module Gelbooru
    require 'imageboard'
    require 'open-uri'
    require 'nokogiri'

    Imageboard.board :gelbooru do |args|
        url = 'http://gelbooru.com/index.php?page=dapi&s=post&q=index&tags='
        url = URI.escape("#{url}#{args[:tags].join('+')}")
        tmp = []

        begin
            doc = Nokogiri::XML(open(url))
            tmp = doc.xpath('//post').map do |elem|
                elem.attribute('file_url').value
            end
        rescue OpenURI::HTTPError
            puts "failed to retrieve url:\n#{url}"
        end

        tmp
    end
end
