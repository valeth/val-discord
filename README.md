# VAL Discord Bot

[![Code Climate](https://codeclimate.com/github/valeth/val-discord/badges/gpa.svg)](https://codeclimate.com/github/valeth/val-discord)

Command bot for discord


## Setup
run `bundle install` to install all required dependencies

> the `pg` gem requires postgresql to be installed

export at least the following environment variables

```shell
DISCORD_TOKEN   # discord API token
DISCORD_APPID   # discord client id
```

> You can export the `DISCORD_PREFIX` variable to change the bot's command prefix.
> This can be useful if a bot is already running and you are just testing stuff
> with a dedicated development bot for example.
> Per default the command prefix will be `!`.

Also, make sure a postgresql server is running locally.

Alternatively you can set the `DATABASE_URL` variable.

> All configuration options can also be configured in their
> respective config files.

After everything is set, run `rake run` to start the bot
or `rake console` for an interactive shell.
