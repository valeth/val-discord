# frozen_string_literal: true

require 'google_url_shortener'

class VAL::Plugins::Links
    GOOGLE_API_KEY = VAL::Config[:apis][:google]
    URLShortener = Google::UrlShortener
    URLShortener::Base.api_key = GOOGLE_API_KEY

    def cmd_links(event)
        server = event.server
        tables = get_link_tables(server)
        return 'No links on this server, sorry.' if tables.empty?

        msg = "```markdown\n"
        msg += "#{server.name} links\n"
        msg += "#{'-' * (server.name.size + 6)}\n\n"
        msg += '```'

        event.respond(msg)

        tables.each do |table|
            event.respond(table)
        end

        nil
    end

    private

    def shorten_url(url)
        @link_cache ||= {}

        @link_cache.fetch(url) do |u|
            link = URLShortener::Url.new(long_url: u).shorten!
            @link_cache[url] = link
        end
    end

    def get_shortened_links(server)
        links = Link.where(server_id: server.id)

        links.map.with_index(1) do |l, i|
            url = shorten_url(l.link)
            { i: i, desc: l.description, url: url }
        end
    end

    def get_link_tables(server)
        links = get_shortened_links(server)
        return [] if links.empty?
        width = links.max_by { |x| x[:desc].size }[:desc].size

        sets = VAL::Utils.distribute(links, 5)
        fmt = "%-3{i}  |  %-#{width}{desc}  |  %{url}"

        sets.map do |set|
            msg = []
            msg << "```\n"
            msg << set.map { |x| format(fmt, x) }.join("\n")
            msg << "```\n"
            msg.join
        end
    end
end
