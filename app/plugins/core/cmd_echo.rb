# frozen_string_literal: true

class VAL::Plugins::Core
    def cmd_echo(_event, *args)
        args.join(' ')
    end
end
