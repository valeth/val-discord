# frozen_string_literal: true

module Discordrb
    class Channel
        DEFAULT_MESSAGE_TIMEOUT = 30

        def send_temporary_embed(message = '', timeout = nil, embed = nil)
            timeout ||= DEFAULT_MESSAGE_TIMEOUT
            embed   ||= Discordrb::Webhooks::Embed.new
            yield(embed) if block_given?
            send_temporary_message(message, timeout, false, embed)
        end
    end
end
