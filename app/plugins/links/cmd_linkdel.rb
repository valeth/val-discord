# frozen_string_literal: true

class VAL::Plugins::Links
    def cmd_linkdel(event, *desc)
        server_id = event.server.id

        link = Link.find_by(description: desc.join(' '), server_id: server_id)
        link.destroy

        'Link removed.'
    end
end
